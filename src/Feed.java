import java.util.ArrayList;
import java.util.List;

/**
 * Klasa przechowuje feed RSS
 */
public class Feed {

    final String title;
    final String description;

    final List<FeedMessage> entries = new ArrayList<FeedMessage>();

    public Feed(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public List<FeedMessage> getMessages() {
        return entries;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Feed [title=" + title + ", description=" + description + "]";
    }
}