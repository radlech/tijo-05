/**
 * Klasa glowna
 */
public class Main {

    // Link do kanalu RSS
    public static final String URL = "http://www.rmf24.pl/feed";

    public static void main(String[] args) {

        RSSFeedParser parser = new RSSFeedParser(URL);
        Feed feed = parser.readFeed();
        for (FeedMessage message : feed.getMessages()) {
            System.out.println(message);
        }

    }
}