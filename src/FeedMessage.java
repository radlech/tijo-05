/**
 * Klasa reprezentuje pojedynczą wiadomosc RSS
 */
public class FeedMessage {

    String title;
    String description;

    void setTitle(String title) {
        this.title = title;
    }

    void setDescription(String description) {

        // Usuwanie znacznikow
        if (description.startsWith("<p><a")) {
            int start = description.indexOf("</a>") + 4;
            int stop = description.indexOf("</p>");
            this.description = description.substring(start,stop);
        } else {
            this.description = description;
        }

        // Wyswietlanie cudzoslowow
        this.description = this.description.replaceAll("&quot;","\"");

        // Zawijanie wierszy
        StringBuilder sb = new StringBuilder(this.description);
        int i = 0;
        while ((i = sb.indexOf(" ", i + 90)) != -1) {
            sb.replace(i, i+1, "\n");
        }
        this.description = sb.toString();

    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Tytul: " + title + "\n" +
                "--------------------------------------------------------------------------------------------------------------" +
                "\n" + "Opis: " + description + "\n" +
                "--------------------------------------------------------------------------------------------------------------";
    }
}